// The Template Loader. Used to asynchronously load templates located in separate .html files
var $ = require('jquery');

var Utils = Utils || {};


var tmpl_cache;
var _render = function(tmpl_name) {
    if (!tmpl_cache) {
        tmpl_cache = {};    
    }    
    if (!tmpl_cache[tmpl_name]) {        
        var tmpl_url = '../tpl/' + tmpl_name + '.html';        
        var tmpl_string;        
        $.ajax({            
            url: tmpl_url,
                        method: 'GET',
                        async: false,
                        dataType: "html",
                        success: function(data) {                
                tmpl_string = data;            
            }        
        });        
        tmpl_cache[tmpl_name] = tmpl_string;    
	}else{
		console.log("tmp_cache_working");
	}    
    return tmpl_cache[tmpl_name];
};


var renderLoader = {

    load: function(tmpl_names, callback) {
        $.each(tmpl_names, function(index, tmpl_name) {
            _render(tmpl_name);
        });
		callback("success");
    }
};

Utils._render = _render;
Utils.renderLoader = renderLoader;

module.exports = Utils;
