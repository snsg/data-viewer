var Backbone = require('backbone');
var _ = require("underscore");
var $ = require('jquery');
Backbone.$ = $;

var ContentView = require("./content-view.js");

/**
 * 画面全体
 *
 * @returns {undefined}
 */
var AppView = Backbone.View.extend({

    initialize: function() {
        console.log('Initializing App View');
    },
    render: function() {
		var height = ( window.innerHeight - 200 ) + "px";
		this.$el.css({"height":height});

		this.$el.append(new ContentView().render().el);
        return this;
    }
});
module.exports = AppView;
