var Backbone = require('backbone');
var _ = require("underscore");
var $ = require('jquery');
Backbone.$ = $;

var Utils = require('../utils.js');

/**
 * 画面全体
 *
 * @returns {undefined}
 */
var ContentView = Backbone.View.extend({

    template: _.template(Utils._render("ContentView")),

    initialize: function() {
        console.log('Initializing Content View');
    },
    render: function() {
        $(this.el).html(this.template);
        return this;
    }
});
module.exports = ContentView;
