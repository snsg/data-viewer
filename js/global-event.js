var Backbone = require('backbone');
var _ = require("underscore");

var globalEvents = {};
_.extend(globalEvents, Backbone.Events);

//debug
globalEvents.on("all", function(e){
	console.log("event: "+ e + " triggered");
});

module.exports = globalEvents;
