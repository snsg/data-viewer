var Backbone = require('backbone');
var _ = require("underscore");
var $ = require('jquery');
Backbone.$ = $;

var RecipeModel = Backbone.Model.extend({

    defaults: {
		score:undefined,
		description:undefined
    },
	parse: function(response, options){
		console.log("model parse");
		console.log(response);
		var tmp={};
		tmp.score = response.score;
		tmp.description = "dummy text";
		tmp.if_service = response.template.if[0].service;
		console.log(tmp);
		return tmp;
	}
});


module.exports = RecipeModel;
