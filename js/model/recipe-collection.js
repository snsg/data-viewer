var Backbone = require('backbone');
var _ = require("underscore");
var $ = require('jquery');
Backbone.$ = $;

var RecipeModel = require('./recipe-model.js');

/**
 * 
 * レシピのコレクション
 * 
 */
var RecipeCollection = Backbone.Collection.extend({

	initialize: function(models, options){
		console.log("RecipeCollection Initializing");
		this.myurl = options.url;
	},

    model: RecipeModel,
    // url: '../../recipes.json'
	url: function() {
		// return '../../recipes.json';
		return this.myurl;
	},

	/**
	 * urlから取得したデータのパース
	 * 
	 */
	parse:function(res){
		console.log("recipe collection parse");
		console.log(res);
		return res.response.data;
	},

	add: function(e){
		console.log("collection add called");
		console.log(e);
		console.log(JSON.stringify(e.attributes));
		this.save(e);
	},

	save: function(e){
		console.log("recipe collection save");
		console.log(e.attributes);
		$.post(this.url(),e.attributes,undefined,'json');
	}
});

module.exports = RecipeCollection;
