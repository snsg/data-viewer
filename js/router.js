var AppView = require('./view/app-view.js');
var Backbone = require("backbone");
var $ = require('jquery');

var Router = Backbone.Router.extend({

    routes: {
        "": "app"
    },

    initialize: function () {
		console.log("init router");
    },

	app : function(){
		this.AppView = new AppView();
        $("#page-wrapper").append(this.AppView.render().el);
	}
});

module.exports = Router;
