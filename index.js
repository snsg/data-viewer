// https://github.com/chrisdickinson/beefy
//
var Backbone = require("backbone");
var $ = require('jquery');

var Router = require('./js/router');

$(document).ready(function() {
	app = new Router();
    Backbone.history.start();
});
